/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waratchaya.databaseproject.dao;

import com.waratchaya.databaseproject.helper.DatabaseHelper;
import com.waratchaya.databaseproject.model.OrderDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Melon
 */
public class OrderDetailDao implements Dao<OrderDetail> {

    @Override
    public OrderDetail get(int id) {
        OrderDetail item = null;
        String sql = "SELECT * FROM order_detail WHERE order_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = OrderDetail.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    public List<OrderDetail> getAll() {
        ArrayList<OrderDetail> list = new ArrayList();
        String sql = "SELECT * FROM order_detail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OrderDetail item = OrderDetail.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<OrderDetail> getAll(String where, String order) {
        ArrayList<OrderDetail> list = new ArrayList();
        String sql = "SELECT * FROM order_detail where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OrderDetail item = OrderDetail.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<OrderDetail> getByOrdersId(int orderId) {
        return getAll("orders_id = " + orderId, " order_detail_id ASC ");
    }

    public List<OrderDetail> getAll(String order) {
        ArrayList<OrderDetail> list = new ArrayList();
        String sql = "SELECT * FROM order_detail ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OrderDetail item = OrderDetail.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public OrderDetail save(OrderDetail obj) {
        String sql = "INSERT INTO order_detail (product_id, qty, product_price, product_name, order_id)" + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getProduct().getId());
            stmt.setInt(2, obj.getQty());
            stmt.setDouble(3, obj.getProductPrice());
            stmt.setString(4, obj.getProductName());
            stmt.setInt(5, obj.getOrder().getId());
            //System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public OrderDetail update(OrderDetail obj) {
        //  String sql = "UPDATE order_detail"
        //        + "   SET item_name = ?, item_gender = ?, item_password = ?, item_role = ?"
        //      + " WHERE item_id = ? ";
        //    Connection conn = DatabaseHelper.getConnect();
        //  try {
        //    PreparedStatement stmt = conn.prepareStatement(sql);
        //  stmt.setString(1, obj.getName());
        //stmt.setString(2, obj.getGender());
        //     stmt.setString(3, obj.getPassword());
        //    stmt.setInt(4, obj.getRole());
        //    stmt.setInt(5, obj.getId());
        //    //System.out.println(stmt);
        //    int ret = stmt.executeUpdate();
        //  System.out.println(ret);
        // return obj;
        //  } catch (SQLException ex) {
        //    System.out.println(ex.getMessage());
        //  return null;
        // }
        return null;
    }

    @Override
    public int delete(OrderDetail obj) {
        String sql = "DELETE  FROM order_detail WHERE _id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
