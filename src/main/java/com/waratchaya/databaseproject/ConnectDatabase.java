/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waratchaya.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Melon
 */
public class ConnectDatabase {
    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:dcoffee3.db";
        try{
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been estabilsh.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }finally {
            if(conn!=null) {
                try {
                    conn.close();
                }catch(SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }
    }
}
